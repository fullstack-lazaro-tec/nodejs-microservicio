const fs = require('fs');
const path = require('path');
const http = require('http');

http.createServer(function(request, response){
    console.log('request', request.url);

    let filePath = `.${request.url}`;
    if(filePath == './'){
        filePath = './index.html';
    }

    let extname = String(path.extname(filePath)).toLowerCase();
    let contentType = 'text/html';
    let mimeTypes = {
        '.css': 'text/css',
        '.png': 'image/png',
        '.html': 'text/html',
        '.js': 'text/javascript'
    };

    contentType = mimeTypes[extname] || 'application/octet-stream';

    fs.readFile(filePath, function(error, content){
        if(error){
            if(error.code == 'ENOENT'){
                fs.readFile('./404.html', function(error, content){
                    response.writeHead(200, {'Content-Type': contentType});
                    response.end(content, 'utf-8');
                });
            }else{
                response.writeHead(500);
                response.end(`Sorry, check with the site admin for error: ${error.code} ...\n`);
                response.end();                
            }
        }else{
            response.writeHead(200, {'Content-Type': contentType});
            response.end(content, 'utf-8');
        }
    });
}).listen(3000);
console.log('Server runing at http:127.0.0.1:3000/');